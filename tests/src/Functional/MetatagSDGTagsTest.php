<?php

  namespace Drupal\Tests\metatag_sdg\Functional;

use Drupal\Tests\metatag\Functional\MetatagTagsTestBase;

/**
 * Tests that each of the Dublin Core tags work correctly.
 *
 * @group metatag
 */
class MetatagSDGTagsTest extends MetatagTagsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['metatag_sdg'];

  /**
   * {@inheritdoc}
   */
  protected $tags = [
    'DC.ISO3166' => 'sdg_countrycode',
    'DC.Service' => 'sdg_contenttype',
    'sdg-tag' => 'sdg_tag',
    'DC.Location' => 'sdg_location',
    'DC.Policy' => 'sdg_classificationdescription',
    'policy-code' => 'sdg_classificationcode',
  ];

  /**
   * Each of these meta tags has a different tag name vs its internal name.
   */
  protected function getTestTagName($tag_name) {

    return array_search($tag_name, $this->tags);
  }

}
