<?php

namespace Drupal\metatag_sdg\Plugin\metatag\Group;

use Drupal\metatag\Plugin\metatag\Group\GroupBase;

/**
 * The advanced group.
 *
 * @MetatagGroup(
 *   id = "sdg",
 *   label = @Translation("SDG"),
 *   description = @Translation("Meta tags for SDG"),
 *   weight = 2
 * )
 */
class Sdg extends GroupBase {
  // Inherits everything from Base.
}
