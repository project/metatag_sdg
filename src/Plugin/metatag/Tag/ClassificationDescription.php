<?php

namespace Drupal\metatag_sdg\Plugin\metatag\Tag;

use Drupal\metatag\Plugin\metatag\Tag\MetaNameBase;

/**
 * The SDG "Classification information description" meta tag.
 *
 * @MetatagTag(
 *   id = "sdg_classificationdescription",
 *   label = @Translation("Classification information description"),
 *   description = @Translation("Information about the code of the content area covered by the page according to Annex I and II or the full name of the assistance service in Annex III."),
 *   name = "DC.Policy",
 *   group = "sdg",
 *   weight = 5,
 *   type = "label",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class ClassificationDescription extends MetaNameBase {
  // Nothing here yet. Just a placeholder class for a plugin.
}
