<?php

namespace Drupal\metatag_sdg\Plugin\metatag\Tag;

use Drupal\metatag\Plugin\metatag\Tag\MetaNameBase;

/**
 * The SDG Tag meta tag.
 *
 * @MetatagTag(
 *   id = "sdg_tag",
 *   label = @Translation("Sdg-Tag"),
 *   description = @Translation("SDG text identifying the page as part of the Single Digital Gateway"),
 *   name = "sdg-tag",
 *   group = "sdg",
 *   weight = 4,
 *   type = "label",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SdgTag extends MetaNameBase {
  // Nothing here yet. Just a placeholder class for a plugin.
}
