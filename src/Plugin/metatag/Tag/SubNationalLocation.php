<?php

namespace Drupal\metatag_sdg\Plugin\metatag\Tag;

use Drupal\metatag\Plugin\metatag\Tag\MetaNameBase;

/**
 * The SDG "Sub-national location" meta tag.
 *
 * @MetatagTag(
 *   id = "sdg_location",
 *   label = @Translation("Sub-national location"),
 *   description = @Translation("The NUTS or LAU location id for which the content on the page is valid."),
 *   name = "DC.Location",
 *   group = "sdg",
 *   weight = 6,
 *   type = "label",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SubNationalLocation extends MetaNameBase {
  // Nothing here yet. Just a placeholder class for a plugin.
}
