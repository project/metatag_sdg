<?php

namespace Drupal\metatag_sdg\Plugin\metatag\Tag;

use Drupal\metatag\Plugin\metatag\Tag\MetaNameBase;

/**
 * The SDG "Content type" meta tag.
 *
 * @MetatagTag(
 *   id = "sdg_contenttype",
 *   label = @Translation("Content type"),
 *   description = @Translation("It will contain the information about the type of content present on the page"),
 *   name = "DC.Service",
 *   group = "sdg",
 *   weight = 5,
 *   type = "label",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class ContentType extends MetaNameBase {
  // Nothing here yet. Just a placeholder class for a plugin.
}
