<?php

namespace Drupal\metatag_sdg\Plugin\metatag\Tag;

use Drupal\metatag\Plugin\metatag\Tag\MetaNameBase;

/**
 * The SDG "Classification information code" meta tag.
 *
 * @MetatagTag(
 *   id = "sdg_classificationcode",
 *   label = @Translation("Classification information code"),
 *   description = @Translation("Information about the code of the content area covered by the page according to Annex I and II or the full name of the assistance service in Annex III."),
 *   name = "policy-code",
 *   group = "sdg",
 *   weight = 5,
 *   type = "label",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class ClassificationCode extends MetaNameBase {
  // Nothing here yet. Just a placeholder class for a plugin.
}
