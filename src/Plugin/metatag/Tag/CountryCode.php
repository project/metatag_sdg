<?php

namespace Drupal\metatag_sdg\Plugin\metatag\Tag;

use Drupal\metatag\Plugin\metatag\Tag\MetaNameBase;

/**
 * The SDG "Country Code" meta tag.
 *
 * @MetatagTag(
 *   id = "sdg_countrycode",
 *   label = @Translation("Country Code"),
 *   description = @Translation("Contain the two characters ISO 3166-1 representation of names of countries"),
 *   name = "DC.ISO3166",
 *   group = "sdg",
 *   weight = 5,
 *   type = "label",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class CountryCode extends MetaNameBase {
  // Nothing here yet. Just a placeholder class for a plugin.
}
